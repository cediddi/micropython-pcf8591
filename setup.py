from setuptools import setup

import optimize_upip

setup(
        name='micropython-pcf8591',
        version='0.1.1',
        description='A pcf8591 driver for micropython',
        long_description='PCF8591 is a digital-analog and analog-digital converter that supports 4 analog inputs'
                         ' and 1 analog output and a I2C interface. This driver is for MicroPython and microchips'
                         ' that have a I2C interface.',
        author='Umut Karcı',
        author_email='cediddi@gmail.com',
        license='MIT',
        cmdclass={'optimize_upip': optimize_upip.OptimizeUpip},
        py_modules=['pcf8591'],
        url='https://gitlab.com/cediddi/micropython-pcf8591/'
)
