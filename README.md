# PCF8591 for Micropython

## Installation

It's actually very simple. Just import upip and install the package.

    import upip
    upip.install('micropython-pcf8591')

## Usage

The PCF8591 class has 4 main public methods available: set_out, set_program, read, write.

### PCF8591:

#### \_\_init\_\_(i2c, addr=..., enable_out=..., in_program=...):

_Initializes the instance_

- **i2c** is a Micropython I2C instance
- **addr** is the address of the device, defaults to 72 (0x48).
- **enable_out** is for setting "ANALOG OUTPUT ENABLE FLAG". Also enables the internal oscillator which is a must for auto-increment. Valid options are: (`True`, `False`). Defaults to `True`.
- **in_program** is for settings "ANALOG INPUT PROGRAMMING". Valid options are: (`AINPRG0`, `AINPRG1`, `AINPRG2`, `AINPRG3`). Defaults to `AINPRG0`.

#### set_out(enable_out):

_Sets the output flag_

- **enable_out** is for setting "ANALOG OUTPUT ENABLE FLAG". Also enables the internal oscillator which is a must for auto-increment. Valid options are: (`True`, `False`).

#### set_program(in_program):

_Sets the input program_

- **in_program** is for settings "ANALOG INPUT PROGRAMMING". Valid options are: (`AINPRG0`, `AINPRG1`, `AINPRG2`, `AINPRG3`).

#### read(channel=...):

_Reads analog data from the device. When auto incrementing is enabled, this function will return all four inputs as a list, otherwise returns a single integer._

- **channel** is for setting the "A/D CHANNEL NUMBER". When is None, auto incrementing will be enabled. Valid options are(`ACHNNL0`, `ACHNNL1`, `ACHNNL2`, `ACHNNL3`, `None`). Defaults to `None`.

#### write(value):

_Writes analog data to the device. It'll automatically enable analog out._

- **value** is for setting the analog output value of the device. Valid options are: (`0-255`)

### Constants

- **AINPRG0**: ANALOG INPUT PROGRAM 0
- **AINPRG1**: ANALOG INPUT PROGRAM 1 
- **AINPRG2**: ANALOG INPUT PROGRAM 2
- **AINPRG3**: ANALOG INPUT PROGRAM 3
- **ACHNNL0**: ANALOG CHANNEL 0
- **ACHNNL1**: ANALOG CHANNEL 1
- **ACHNNL2**: ANALOG CHANNEL 2
- **ACHNNL3**: ANALOG CHANNEL 3

## Development

As you can guess, there are no tests whatsoever, no coverage, nothing!
You can create a distribution package by running this command inside pcf8591 directory.

    python setup.py sdist optimize_upip